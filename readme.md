# @lpgroup/docs

MDX parser library that returns styled JSX

## Contents

- [Prerequisites](#prerequisites)
- [Install](#install)
- [API](#api)
- [Contribute](#custom)
- [License](#license)

## Prerequisites

Requires plugin to use MDX. Example in Vite setup, `vite.config.js`

```sh
import { defineConfig } from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import mdx from "vite-plugin-mdx";

export default defineConfig({
  plugins: [
    reactRefresh(),
    mdx()
]
})
```

## Install

```sh
# via yarn
yarn add @lpgroup/docs

# via npm
npm install --save @lpgroup/docs
```

## API

## `ParseMDXToFullDocs`

This will convert MDXs to documentation format, clickable sidebar in the left and styled jsx in the right.

```sh
import React from 'react'
import ParseMDXToFullDocs from '@lpgroup/docs';
import HelloWorld1 from "./fiel1.mdx";
import HelloWorld2 from "./file2.mdx";
import HelloWorld3 from "./file3.mdx";

const Pages = () =>{
    return(
    <>
        <HelloWorld1/>
        <HelloWorld2/>
        <HelloWorld3/>
    </>
    )
}

const logo =() => <div>MyLogo</div>

function App() {
  return (
    <div>
      <ParseMDXToFullDocs
      drawerWidth={240}
      logo={logo}
      logoClassName={{margin: "10px"}}
      >
        <Pages />
      </ParseMDXToFullDocs>
    </div>
  )
}
```

| property        | type, default     | description             |
| --------------- | ----------------- | ----------------------- |
| `drawerWidth`   | `number, 240`     | the width of the drawer |
| `logo`          | `function`, <></> | Logo                    |
| `logoClassName` | `object`, {}      | Style of the logo       |

## `MDXFormat`

This is similar to `ParseMDXToFullDocs`, but without the sidebar, also will receive a props that will modify its display.

```sh
import {MDXFormat, mdxComponents} from '@lpgroup/docs'
const defineComponents = {
    ...mdxComponents,
    h1: (props) => <div style={{backgroundColor: "yellow", fontSize: "30px"}} {...props}/>
  }
```

```sh
     <MDXFormat
      customRender={defineComponents}
      elementTags={["h1", "h2", "p"]}
      >
        <Pages />
      </MDXFormat>
```

| property       | type, default          | description                              |
| -------------- | ---------------------- | ---------------------------------------- |
| `customRender` | object, `mdxComponent` | customize the component render           |
| `elementTags`  | array, ["all"]         | will only show the elements in the array |

Possible element tags; "h1", "h2", "h3", "h4", "p", "li", "inlineCode", "code" "pre", "table", "all". The default is "all"

## `TagsArrayFilter`

This will receive an array elementTags ["all"] and return a filtered mdx.

```sh
<MDXRootProvider>
    <TagsArrayFilter elementTags={["h2"]} >
        <Pages />
    </TagsArrayFilter>
</MDXRootProvider>
```

## `MDXRootProvider`

Will allow the elements from mdx to be modified, all external styles are remove.

```sh
import {MDXRootProvider, mdxComponents} from '@lpgroup/docs'
const defineComponents = {
    ...mdxComponents,
    h1: (props) => <div style={{backgroundColor: "yellow", fontSize: "30px"}} {...props}/>
  }
```

```sh
<MDXRootProvider customRender={defineComponents}>
    <Pages/>
<MDXRootProvider>
```

default customRender is the `mdxComponents`

## `MDXProvider`

```sh
import {MDXProvider} from '@lpgroup/docs'
```

See [usage](https://mdxjs.com/getting-started#mdxprovider)

## `mdxComponents`

```sh
import {mdxComponents} from '@lpgroup/docs'
```

```sh
const components = {...mdxComponents, h1: ()=> }
```

This is default object that is used in the `MDXRootProvider`

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
