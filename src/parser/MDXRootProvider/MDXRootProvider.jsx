import React from "react";
import { MDXProvider } from "@mdx-js/react";
import { mdxComponents } from "../../components";

export const MDXRootProvider = ({ children, customRender = mdxComponents }) => {
  return <MDXProvider components={customRender}>{children}</MDXProvider>;
};


