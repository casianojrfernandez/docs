import React from "react";
import { MDXProvider } from "@mdx-js/react";
import { CssBaseline } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { SideBar, Theme, mdxComponents, InfoProvider } from "../../components";

export default function ParseMDXToFullDocs({
  children,
  drawerWidth = 240,
  logoClassName = {},
  logo = () => null,
}) {
  const useStyles = makeStyles((theme) => ({
    root: {
      [theme.breakpoints.up("sm")]: {
        marginLeft: drawerWidth,
      },
    },
    toolbar: {
      [theme.breakpoints.down("xs")]: theme.mixins.toolbar,
    },
    content: {
      padding: theme.spacing(6, 3),
    },
  }));

  const classes = useStyles();
  return (
    <Theme>
      <InfoProvider drawerWidth={drawerWidth} logo={logo} logoClassName={logoClassName}>
        <div className={classes.root}>
          <MDXProvider components={mdxComponents}>
            <CssBaseline />
            <SideBar>{children}</SideBar>
            <div className={classes.toolbar} />
            <div className={classes.content}>{children}</div>
          </MDXProvider>
        </div>
      </InfoProvider>
    </Theme>
  );
}

