/* 

ParseMDXToFullDocs 
    exported as DEFAULT
    This will convert MDXs to Documetation Format
    clickable SideBar on the left, styled components on the right

MDXFormat
    This is similar to ParseMDXToFullDocs, but without the sidebar, 
    also will receive a props customRender; {}, and elementTags; [] as filter.

TagsArrayFilter
    Will receive array element tag ["all", "h1", "h2"] and return a filtered mdx
    <MDXRootProvider>
    <TagsArrayFilter elementTags={["h2"]} >
    <Pages />
    </TagsArrayFilter>
    </MDXRootProvider>

mdxComponents
    a well styled mdxComponent can be integrated in MDXProvider, MDXFormat

MDXRootProvider
    Will allow the elements from mdx to be modified, all external styles are remove.
    default customRender is the mdxComponents

*/

import ParseMDXToFullDocs from "./parser/ParseMDXToFullDocs/ParseMDXToFullDocs";

// export { MDXFormat, TagsArrayFilter } from "./parser/MDXFormat";
// export { MDXRootProvider } from "./parser/MDXRootProvider";
// export { MDXProvider } from "@mdx-js/react";
// export { mdxComponents } from "./components";
export default ParseMDXToFullDocs;
