import React from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Section } from "./Section";
import { formatAnchorId } from "../utils";
import { Example, Attributes, Attribute } from "../mdx";
import { CodeBlock } from "./CodeBlock";

function wrapper({ children, ...props }) {
  const contents = [];
  let header = [];
  let leftColumn = [];
  let rightColumn = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const child of children) {
    const hasMdxType = child && child.props && child.props.mdxType;
    if (hasMdxType && ["h1", "h2"].includes(child.props.mdxType)) {
      contents.push(
        <Section
          key={contents.length + 1}
          header={[...header]}
          leftColumn={[...leftColumn]}
          rightColumn={[...rightColumn]}
        />
      );
      header = [];
      leftColumn = [];
      rightColumn = [];

      header.push(child);
    } else if (hasMdxType && ["Example", "Request", "pre"].includes(child.props.mdxType)) {
      rightColumn.push(child);
    } else {
      leftColumn.push(child);
    }
  }

  contents.push(
    <Section
      key={contents.length + 1}
      header={[...header]}
      leftColumn={[...leftColumn]}
      rightColumn={[...rightColumn]}
    />
  );

  return contents;
}

const useStyles = makeStyles((theme) => ({
  li: {
    margin: 0,
  },
  inlineCode: {
    color: "#2a2f45",
    backgroundColor: "rgba(0, 0, 0, 0.05)",
    border: `1px solid ${theme.palette.divider}`,
  },
  table: {
    "margin": theme.spacing(2),
    "borderCollapse": "collapse",

    "& tr:last-child": {
      borderSpacing: "10px",
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
  },
  th: {
    padding: theme.spacing(1),
    whiteSpace: "nowrap",
    textAlign: "left",
  },
  td: {
    padding: theme.spacing(1),
  },
}));

// https://mdxjs.com/getting-started#table-of-components
export const mdxComponents = {
  wrapper,
  p: (props) => <Typography variant="body1" {...props} />,
  h1: (props) => <Typography variant="h1" {...props} id={formatAnchorId(props.children)} />,
  h2: (props) => <Typography variant="h2" {...props} id={formatAnchorId(props.children)} />,
  h3: (props) => <Typography variant="h3" {...props} />,
  h4: (props) => <Typography variant="h4" {...props} />,

  li: (props) => {
    const classes = useStyles();
    return (
      <li>
        <Typography variant="body1" className={classes.li} {...props} />
      </li>
    );
  },

  inlineCode: (props) => {
    const classes = useStyles();
    return <code className={classes.inlineCode} {...props} />;
  },

  table: (props) => {
    const classes = useStyles();
    return <table className={classes.table} {...props} />;
  },

  th: (props) => {
    const classes = useStyles();
    return <th className={classes.th} {...props} />;
  },

  td: (props) => {
    const classes = useStyles();
    return <td className={classes.td} {...props} />;
  },

  code: CodeBlock,
  pre: (props) => <div {...props} />,

  // shortcodes - react elements
  Attribute,
  Attributes,
  Example,
};
