import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { AppBar, Drawer, Hidden } from "@material-ui/core/";
import { NavContainer, MenuContainer, MenuItem } from "./TableOfContents";
import { AppToolBar } from "./AppToolBar";
import { useInfo } from "./InfoProvider";

/**
 * Generate array with only `type` react elements
 */
function filterHeader(children, type) {
  return React.Children.map(children, (child) =>
    child && child.props && child.props.mdxType === type ? child : undefined
  );
}

/**
 * Parse each mdx file for h1, h2 tags and generate MenuContainer and MenuItems
 * WARNING: Only supports one h1 tag in each mdx file.
 */
function filterHeaders(children) {
  if (!children) return;
  const h1 = filterHeader(children, "h1");
  const h2 = filterHeader(children, "h2");

  const MenuItems = React.Children.map(h2, (child) =>
    child.props.children ? <MenuItem title={child.props.children} /> : undefined
  );
  // eslint-disable-next-line consistent-return
  return <MenuContainer title={h1[0].props.children}>{MenuItems}</MenuContainer>;
}

/**
 * Loops each mdx file
 */
function getMenuContainers(props) {
  // props.children == <pages>
  const childrenOfPages = props.children.type({}).props.children;
  return React.Children.map(childrenOfPages, (child) => {
    const content = child.type({}).props.children;
    return filterHeaders(content);
  });
}

export function SideBar(props) {
  const { drawerWidth } = useInfo();
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
    },
    drawer: {
      [theme.breakpoints.up("sm")]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },

    appBar: {
      borderBottom: `0px solid ${theme.palette.divider}`,
      boxShadow: "none",
      [theme.breakpoints.up("sm")]: {
        display: "none",
      },
    },

    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }));
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container = window !== undefined ? () => window().document.body : undefined;
  const drawer = <NavContainer>{getMenuContainers(props)}</NavContainer>;

  return (
    <>
      <AppBar position="fixed" color="inherit" className={classes.appBar}>
        <AppToolBar handleDrawerToggle={handleDrawerToggle} />
      </AppBar>

      <nav className={classes.drawer}>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            <AppToolBar handleDrawerToggle={handleDrawerToggle} />
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            <AppToolBar handleDrawerToggle={handleDrawerToggle} />
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </>
  );
}
