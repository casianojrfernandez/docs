import React from "react";
import { Collapse, Link, ListItemText, ListItem, List } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { formatAnchorId } from "../utils";

const useStyles = makeStyles((theme) => ({
  menuRoot: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  text: {
    fontSize: 14,
    fontWeight: 600,
  },
  nested: {
    marginTop: 0,
    paddingLeft: theme.spacing(3),
    paddingTop: theme.spacing(0),
    paddingBottom: theme.spacing(0),
  },
  nestedText: {
    marginTop: 0,
    fontSize: 14,
  },
  toolbarLink: {
    color: "inherit",
  },
}));

function jump(title) {
  // eslint-disable-next-line no-restricted-globals
  location.href = `#${title}`;
}

export function NavContainer(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(null);

  const handleChange = (panel) => () => {
    setExpanded(panel);
    jump(panel);
  };

  const childrenWithProps = React.Children.map(props.children, (child) =>
    React.cloneElement(child, {
      onClick: handleChange,
      expanded,
    })
  );

  return (
    <List component="nav" className={classes.menuRoot}>
      {childrenWithProps}
    </List>
  );
}

export function MenuContainer(props) {
  const classes = useStyles();
  const anchorId = formatAnchorId(props.title);
  const open = props.expanded === anchorId;
  const numOfChildren = React.Children.count(props.children);
  return (
    <>
      <ListItem button onClick={props.onClick(anchorId)}>
        <ListItemText primary={props.title} disableTypography className={classes.text} />
        {numOfChildren !== 0 && (open ? <ExpandLess /> : <ExpandMore />)}
      </ListItem>
      <Collapse in={open} timeout="auto">
        <List component="div" disablePadding>
          {props.children}
        </List>
      </Collapse>
    </>
  );
}

export function MenuItem(props) {
  const classes = useStyles();

  // TODO: The href should be on the ListItem
  return (
    <ListItem
      button
      className={classes.nested}
      component={Link}
      key={props.title}
      href={`#${formatAnchorId(props.title)}`}
    >
      <ListItemText primary={props.title} disableTypography className={classes.nestedText} />
    </ListItem>
  );
}
