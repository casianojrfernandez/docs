import React, { createContext, useContext } from "react";

const InfoContext = createContext(null);

export function useInfo() {
  return useContext(InfoContext);
}

export function InfoProvider({ children, drawerWidth, logo, logoClassName }) {
  const value = { drawerWidth, logo, logoClassName };

  return <InfoContext.Provider value={value}>{children}</InfoContext.Provider>;
}
