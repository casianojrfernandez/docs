import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";

// TODO: Until next version of Material-ui
import { unstable_createMuiStrictModeTheme as createMuiTheme } from "@material-ui/core";

const docTheme = createMuiTheme({
  typography: {
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
});

docTheme.typography.h1 = {
  fontSize: "2rem",
  fontWeight: 400,
  marginBottom: docTheme.spacing(2),
};

docTheme.typography.h2 = {
  fontSize: "2rem",
  fontWeight: 400,
  marginBottom: docTheme.spacing(2),
};

docTheme.typography.h3 = {
  fontSize: "1.25rem",
  fontWeight: 500,
  marginTop: docTheme.spacing(2),
  marginBottom: docTheme.spacing(2),
};

docTheme.typography.body1 = {
  lineHeight: "26px",
  fontSize: "16px",
  marginBottom: docTheme.spacing(1),
};

export function Theme(props) {
  return <ThemeProvider theme={docTheme}>{props.children}</ThemeProvider>;
}
