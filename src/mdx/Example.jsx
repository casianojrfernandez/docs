import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 0,
    marginBottom: theme.spacing(4),
    wordBreak: "break-all",
    background: "rgb(250, 250, 250)",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
    color: "#2a2f45",
    backgroundColor: "rgba(0, 0, 0, 0.05)",
  },
  content: {
    overflow: "auto",
    display: "flex",
    flexWrap: "wrap",
    margin: theme.spacing(2, 0),
    paddingTop: 0,
  },
}));

export function Example(props) {
  const classes = useStyles();
  let { title } = props;
  if (props.method === "title") title = title.toUpperCase();
  else title = `${props.method.toUpperCase()} ${title}`;
  return (
    <Card className={classes.root} variant="outlined">
      <CardHeader title={title} className={classes.title} disableTypography />
      <CardContent className={classes.content} style={{ paddingBottom: 0 }}>
        {props.children}
      </CardContent>
    </Card>
  );
}
