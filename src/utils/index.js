export function formatAnchorId(title) {
  return title.replace(/[\W]+/g, "_").toLowerCase() || "#";
}
